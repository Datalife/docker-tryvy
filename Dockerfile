# Dockerfile with:
#   - Buildozer official image
#   - Tryvy recipes
#
# Help:
#   - Before run you must clone recipes on parent folder:
#   hg clone http://gitlab.com/datalifeit/tryvy-recipes ../recipes
#
# Run container to generate apk:
#   docker run --volume "$PWD":/home/user/hostcwd --volume "buildozer:/home/user/.buildozer" --volume "$PWD"/../recipes:/home/user/recipes --rm -it datalife/buildozer-tryvy android debug
# Run container with interactive shell:
#   docker run --volume "$PWD":/home/user/hostcwd --volume "buildozer:/home/user/.buildozer" --volume "$PWD"/../recipes:/home/user/recipes --rm --entrypoint /bin/bash -it datalife/buildozer-tryvy

FROM kivy/buildozer
LABEL org.label-schema.version="-tryvy"

ENV USER="user"

USER root
RUN apt update -qq > /dev/null \
    && DEBIAN_FRONTEND=noninteractive apt install -qq --yes --no-install-recommends \
    build-essential \
    zlib1g-dev \
    libncurses5-dev \
    libgdbm-dev \
    libnss3-dev \
    libreadline-dev \
    libffi-dev \
    libssl-dev \
    wget

USER ${USER}
RUN pip3 install --upgrade pip buildozer --user
RUN pip3 install --user pyopenssl pyssl

# change permissions to .buildozer folder
WORKDIR ${HOME_DIR}
RUN mkdir .buildozer && chown user:user .buildozer

WORKDIR ${WORK_DIR}

VOLUME ["/home/user/.buildozer", "/home/user/recipes"]